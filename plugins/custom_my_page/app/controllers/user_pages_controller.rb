class UserPagesController < ApplicationController
  unloadable

  # POST /user_page
  def update
    @user_page = UserPage.where(user_id: params[:user_id]).first
    if @user_page
      respond_to do |format|
        if @user_page.update(user_page_params)
          format.text {render text: 'Saved!'}
        else
          format.text {render text: 'Error!'}
        end
      end
    else
      @user_page = UserPage.new(user_page_params)
      respond_to do |format|
        if @user_page.save
          format.text {render text: 'Saved!'}
        else
          format.text {render text: 'Error!'}
        end
      end
    end
  end

  def index
    @issues = []
    # @limit = per_page_option
    
    manager = User.current
    managers_full_name = 
      if manager.is_a? String
        manager
      elsif manager.is_a? User
        "#{manager.firstname} #{manager.lastname}"
      else
        '_!_!_'
      end
    
    @issues =     
    Issue.where(
      root_id: 
        Issue.select(:id).where(
          assigned_to_id: 
            CustomValue.select(:customized_id).where(
              "customized_type='Principal' and custom_field_id=95 and value='#{managers_full_name}'"),
          parent_id: 
            nil
        )
    ).order(id: :desc)
    render 'user_page/index'
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_page_params
      params.permit(:user_id, :issue_id)
    end
end
