class CreateUserPages < ActiveRecord::Migration
  def change
    create_table :user_pages do |t|
      t.integer :user_id, null: false, index: true
      t.integer :issue_id
    end

  end
end
