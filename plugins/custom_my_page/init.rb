require 'custom_my_page_view_hooks'

Redmine::Plugin.register :custom_my_page do
  name 'Custom MyPage plugin'
  author 'A.Averyanov'
  description 'The plugin allows you to assign associated tasks to users and switch to this task by clicking on the main menu item "My Page"'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'

  menu :top_menu, :user_page, { :controller => 'user_pages', :action => 'index' }, :caption => 'Задачи клиентов', :after => :my_page
end
