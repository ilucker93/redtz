module Custom_my_page
  module Custom_my_page
    class Hooks  < Redmine::Hook::ViewListener
      render_on( :view_users_form, :partial => 'user_page/form')
      render_on( :view_layouts_base_html_head, :partial => 'user_page/mainscript')
    end
  end
end