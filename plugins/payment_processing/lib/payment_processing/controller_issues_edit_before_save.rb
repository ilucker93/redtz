module PaymentProcessing
  class ControllerIssuesBulkBeforeSaveHook < Redmine::Hook::ViewListener
 
    def controller_issues_bulk_edit_before_save(context={})
      set_payment_date(context)
    end

    def controller_issues_edit_before_save(context={})
      set_payment_date(context)
    end

    def set_payment_date(context={})
      if context[:params] && context[:params][:issue]
        if context[:params][:issue][:custom_field_values][Setting.plugin_payment_processing['payment_status_id']] == Setting.plugin_payment_processing['status_paid']
          if context[:issue].custom_field_value(Setting.plugin_payment_processing['payment_date_id']).blank?
            context[:issue].custom_field_values = 
              { Setting.plugin_payment_processing['payment_date_id'] => DateTime.now.to_date }
          end
        end 
      end
      ''
    end

  end
end
