require 'payment_processing/controller_issues_edit_before_save'

Redmine::Plugin.register :payment_processing do
  name 'Payment Processing plugin'
  author 'A.Averyanov'
  description "This plugin sets payment date after payment status changed to 'Paid'"
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'

  settings :default => {
    :payment_status_id => {'empty' => true},
    :status_paid => {'empty' => true},
    :payment_date_id  => {'empty' => true}
  }, :partial => 'settings/payment_id_settings'
end
