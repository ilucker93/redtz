require 'redmine'
require 'patches/issues_helper_path'


Redmine::Plugin.register :copy_params_from_issues do
  name 'Copy Params From Issues plugin'
  author 'Anton Koval'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'
end
