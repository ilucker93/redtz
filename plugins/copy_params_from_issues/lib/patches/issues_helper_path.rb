require_dependency 'issues_helper'

module CopyParamsFromIssues
  module Patches
    module IssuesHelperPatch
     def self.included(base) # :nodoc:     
      base.send(:include, InstanceMethods)
      base.class_eval do
        unloadable
        alias_method_chain :link_to_new_subtask, :additional_attributes
      end
     end

      module InstanceMethods
        def link_to_new_subtask_with_additional_attributes(issue)
          link = link_to_new_subtask_without_additional_attributes(issue)
          first, second = link.split('">')
          attrs = issue.attributes.except('id', 'tracker_id', 'parent_issue_id', 'status_id', 'start_date', 'priority_id').select{ |k, v| v.present?}
          payload = new_project_issue_path(issue.project, :issue => attrs).split('new?').second
          first += payload
          first += '">'
          (first + second).html_safe
        end
      end
    end
  end
end

IssuesHelper.send(:include, CopyParamsFromIssues::Patches::IssuesHelperPatch)

