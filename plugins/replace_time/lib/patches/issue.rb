module ReplaceTime

  module Patches

    module IssuePatch
      
      def self.included(base) # :nodoc:
        unloadable
        base.send(:include, InstanceMethods)  
        
        base.class_eval do
          alias_method_chain :copy_from, :modify_start_date
          before_save :change_time_if_status_changed, if: Proc.new { |issue| issue.status_id_was == 1 && issue.status_id_changed? }       
        end
      end

      module InstanceMethods
        
        def copy_from_with_modify_start_date(arg, options={})
          issue = copy_from_without_modify_start_date(arg, options)
          issue.start_date = Date.current
          issue
        end

        private

        def change_time_if_status_changed
          self.start_date = Date.current
        end

      end

    end

  end

end   

unless Issue.included_modules.include?(ReplaceTime::Patches::IssuePatch)
  Issue.send(:include, ReplaceTime::Patches::IssuePatch)
end
