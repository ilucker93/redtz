Redmine::Plugin.register :replase_time do
  name 'Replace Time plugin'
  author 'Anto Koval'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'
end

require_dependency 'redmine_replace_time'
